package com.mercap.facturaciontelefonica;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FacturaTest {

	Abono abono;
	Abonado abonado;
	Factura factura;
	int mesActual = LocalDate.now().getMonthValue();
	
	@BeforeEach
	void setUp() throws Exception {
		abono = new Abono();
		
		// Creamos un abonado de prueba, con algunas llamadas en su lista.
		abonado = new Abonado("Leandro", abono);
		abonado.nuevaLlamada("4233111", 5);
		abonado.nuevaLlamada("55-112-15646", 10);
		abonado.nuevaLlamada("224-156434", 1);
		
		//Instanciamos la factura del periodo actual (Abril = 4)
		factura = new Factura(abonado, 4);
	}

	@Test
	void costoDeLlamadasNoLocalesEs10con15() {
		assertEquals(factura.getCostoLlamadasNoLocales(),10.15);
	}
	
	// Tests para dia habil de 8 a 20
	
	@Test
	void costoDeLlamadasLocalesEs75Ctvs() {
		assertEquals(factura.getCostoLlamadasLocales(),0.75);
	}
	
	@Test
	void elTotalDeLaFacturaEsDe210con90() {
		assertEquals(factura.getTotal(), 210.90);
	}
	
	// Test para d�as del fin de semana o d�as habiles fuera del rango de 8 a 20
	/*
	@Test
	void costoDeLlamadasLocalesEs50Ctvs() {
		assertEquals(factura.getCostoLlamadasLocales(),0.50);
	}
	
	
	@Test
	void elTotalDeLaFacturaEsDe210con65() {
		assertEquals(factura.getTotal(), 210.65);
	}*/
	

}

package com.mercap.facturaciontelefonica;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class AbonadoTest {
	
	Abonado abonado;
	int mesActual = LocalDate.now().getMonthValue();
	
	@BeforeEach
	void setUp() throws Exception {
		abonado = new Abonado("Leandro", null);
		
		//Llamadas que deber�an entrar como locales
		abonado.nuevaLlamada("4233 1111", 5);
		abonado.nuevaLlamada("42332222", 6);
		abonado.nuevaLlamada("4194899", 20);
		
		//Llamadas que deber�an entrar como no locales
		abonado.nuevaLlamada("55-112-15646", 10);
		abonado.nuevaLlamada("55-112-15646", 10);
		abonado.nuevaLlamada("224-156464", 3);
		abonado.nuevaLlamada("224-156434", 1);
	}

	@Test
	void hayCuatroLlamadasNoLocalesEsteMes() {
		assertEquals(abonado.getLlamadasNoLocales(mesActual).size(), 4);
	}
	
	@Test
	void hayTresLlamadasLocalesEsteMes() {
		assertEquals(abonado.getLlamadasLocales(mesActual).size(), 3);
	}

}

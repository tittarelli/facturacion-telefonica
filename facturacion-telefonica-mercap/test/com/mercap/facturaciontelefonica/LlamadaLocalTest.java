package com.mercap.facturaciontelefonica;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LlamadaLocalTest {
	TipoDeLlamada llamada;
	
	@BeforeEach
	void setUp() {
		llamada = new LlamadaLocal();
	}
	
	
	
	/* Ejecutar seg�n d�a y horario que est�n viendo esto: */ 
	  
	
	
	// Costo de la llamada cuando es fin de semana o d�a habil fuera del rango de 8 a 20.
	@Test
	void costoFinDeSemana() {
		assertEquals(0.10, llamada.getCostoPorMinuto());
	}
	
	/*
	// Costo de la llamada en d�a habil dentro del rango de 8 a 20.
	@Test
	void costoDiaHabilEntre8y20() {
		assertEquals(0.15, llamada.getCostoPorMinuto());
	}
	*/

}

package com.mercap.facturaciontelefonica;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LlamadaInternacionalTest {
	
	TipoDeLlamada llamada;
	
	@BeforeEach
	void setUp() throws Exception {
		llamada = new LlamadaInternacional(55);
	}

	@Test
	void test() {
		assertEquals(1, llamada.getCostoPorMinuto());
	}

}

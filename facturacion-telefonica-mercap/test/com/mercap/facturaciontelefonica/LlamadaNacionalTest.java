package com.mercap.facturaciontelefonica;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class LlamadaNacionalTest {
	TipoDeLlamada llamadaPrueba;
	
	@BeforeEach
	void setUp() throws Exception {
		llamadaPrueba = new LlamadaNacional(224);
	}
	
	@Test
	void costoDeLlamadaNacional() {
		assertEquals(0.15, llamadaPrueba.getCostoPorMinuto());
	}
}

package com.mercap.facturaciontelefonica;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LlamadaTest {
	
	Llamada llamada;
	
	// Una llamada internacional con el c�digo de pa�s 55 y una duraci�n de 5 minutos, cuesta 5 pesos. 
	@Test
	void llamadaInternacional() throws Exception {
		llamada = new Llamada("55-11-33180920", 5); 
		assertEquals(llamada.getCosto(), 5);
	}
	
	// Una llamada Nacional con el c�digo de �rea 224 y una duraci�n de 10 minutos, cuesta 1.5 pesos. 
	@Test
	void llamadaNacional() throws Exception {
		llamada = new Llamada("224-33180920", 10); 
		assertEquals(llamada.getCosto(), 1.5);
	}

}

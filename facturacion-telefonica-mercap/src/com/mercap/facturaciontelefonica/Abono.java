package com.mercap.facturaciontelefonica;

public class Abono {
	private String nombre = "Abono b�sico";
	private double costo = 200;
	
	public Abono() {
	}
	
	//Este polimorfismo por sobrecarga corresponde con la idea de poder crear un abono desde 0. Si no, se utiliza el constructor 
	//por defecto.
	public Abono(String nombre, double costo) {
		this.nombre = nombre;
		this.costo = costo;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public double getCosto() {
		return this.costo;
	}
	
	public void setCosto(double costo) {
		this.costo = costo;
	}
}

package com.mercap.facturaciontelefonica;

import java.util.HashMap;
import java.util.Map;

public class LlamadaInternacional implements TipoDeLlamada {
	private int codigoPais;
	private Map<Integer, Double> costoPorPais = new HashMap<Integer, Double>();
	
	public LlamadaInternacional(int codigoPais) throws Exception {
		//put(clave,valor) sirve para llenar el HashMap. Solo puse un valor a modo de demostraci�n pero con m�s tiempo se podr�a 
		//hacer algo m�s completo y eficiente. 
		costoPorPais.put(55, 1d);
		if (!esNumeroValido(codigoPais)) { throw new Exception("El c�digo de pais no se encuentra registrado en el sistema"); };
		this.codigoPais = codigoPais;
	}
	
	private boolean esNumeroValido(int codigoPais) {
		return costoPorPais.containsKey(codigoPais);
	}
	
	@Override
	public double getCostoPorMinuto() {
		return costoPorPais.get(this.codigoPais);
	}


	@Override
	public boolean esLocal() {
		return false;
	}
	

}

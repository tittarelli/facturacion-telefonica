package com.mercap.facturaciontelefonica;

//Esta clase est� hecha de manera sencilla, cumple con el objetivo de demostrar que las clases y m�todos funcionan como corresponde.
//con m�s tiempo me gustar�a haber hecho una factura con items mucho m�s detallados. 

public class SistemaDeFacturacion {

	private static Abonado abonado;
	private static Abono abono;
	private static Factura factura;
	
	

	public static void main(String[] args) throws Exception {
		setUp();
		imprimirFactura();
	}

	//Este m�todo intenta dar estilo a la factura que se imprime por consola.
	public static void imprimirFactura() {
		System.out.println("Cliente: " + factura.getNombreDeAbonado());
		System.out.println("Factura N� " + factura.getID());
		System.out.println("");
		System.out.println("El costo total por llamadas con destino local es de: $" + factura.getCostoLlamadasLocales());
		System.out.println("El costo total por llamadas con destino fuera del �mbito local es de: $" + factura.getCostoLlamadasNoLocales());
		System.out.println("");
		System.out.print("Sumando su abono b�sico de $" + factura.getBasico() + ", el total de su factura es de: $" + factura.getTotal());
	}
	
	
	public static void setUp() throws Exception {
		abono = new Abono();
		
		// Creamos un abonado de prueba, con algunas llamadas en su lista.
		abonado = new Abonado("Leandro", abono);
		abonado.nuevaLlamada("4233 1111", 5);
		abonado.nuevaLlamada("55-112-15646", 10);
		abonado.nuevaLlamada("224-156434", 1);
		
		//Instanciamos la factura del periodo actual (Abril = 4)
		factura = new Factura(abonado, 4);
	}
}

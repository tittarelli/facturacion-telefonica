package com.mercap.facturaciontelefonica;

import java.util.HashMap;
import java.util.Map;

public class LlamadaNacional implements TipoDeLlamada {
	private int codigoArea;
	private Map<Integer, Double> costoPorLocalidad = new HashMap<Integer, Double>();
	
	public LlamadaNacional(int codigoArea) throws Exception {
		//put(clave,valor) sirve para llenar el HashMap. Solo puse un valor a modo de demostraci�n pero con m�s tiempo se podr�a 
		//hacer algo m�s completo y eficiente. 
		costoPorLocalidad.put(224, 0.15);
		if (!esNumeroValido(codigoArea)) { throw new Exception("El c�digo de �rea no se encuentra registrado en el sistema"); };
		this.codigoArea = codigoArea;
	}
	
	private boolean esNumeroValido(int codigoArea) {
		return costoPorLocalidad.containsKey(codigoArea);
	}
	
	@Override
	public double getCostoPorMinuto() {
		return costoPorLocalidad.get(this.codigoArea);
	}

	@Override
	public boolean esLocal() {
		return false;
	}

}

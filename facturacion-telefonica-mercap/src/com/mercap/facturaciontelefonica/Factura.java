package com.mercap.facturaciontelefonica;

public class Factura {
	private static int idAnterior = 1;
	private int id;
	private Abonado abonado;
	private int periodo;
	
	public Factura(Abonado abonado, int periodo) {
		this.id = idAnterior;
		idAnterior++;
		this.abonado = abonado;
		this.periodo = periodo;
	}
	
	public double getBasico() {
		return abonado.getAbono().getCosto();
	}
	
	public int getID() {
		return this.id;
	}

	
	//Este m�todo convierte la lista de llamadas del cliente en una lista de costos y los suma. 
	public Double getCostoLlamadasLocales() {
		return abonado.getLlamadasLocales(periodo)
				.stream()
				.mapToDouble(Llamada::getCosto)
				.sum();
	}
	
	public Double getCostoLlamadasNoLocales() {
		return abonado.getLlamadasNoLocales(periodo)
				.stream()
				.mapToDouble(Llamada::getCosto)
				.sum();
	}
	
	public double getTotal() {
		return this.getBasico() + this.getCostoLlamadasLocales() + this.getCostoLlamadasNoLocales();
	}

	public String getNombreDeAbonado() {
		return abonado.getNombre();
	}
}

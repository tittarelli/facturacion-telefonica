package com.mercap.facturaciontelefonica;

// Interfaz creada para cumplir con el principio solid de sustituci�n de liskov y que, a su vez, las clases que implementen esta
// interfaz, cumplan con el principio open-closed para poder agregar tiposDeLlamada sin modificar el c�digo existente.

public interface TipoDeLlamada {

	public double getCostoPorMinuto();

	public boolean esLocal();

}

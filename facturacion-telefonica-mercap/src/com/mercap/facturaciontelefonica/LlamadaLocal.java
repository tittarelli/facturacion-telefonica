package com.mercap.facturaciontelefonica;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

public class LlamadaLocal implements TipoDeLlamada {
	private double costoMinutoBasico = 0.10; 
	private double costoMinutoExtraordinario = 0.20;
	
	//Este m�todo compara el d�a actual con s�bado o domingo para conocer si estamos dentro del fin de semana. 
	private boolean esFinDeSemana() {
		DayOfWeek dia = LocalDate.now().getDayOfWeek();
		return dia.equals(DayOfWeek.SATURDAY) || dia.equals(DayOfWeek.SUNDAY);
	}
	
	private boolean esHoraPico() {
		int horaActual = LocalTime.now().getHour();
		return horaActual > 8 && horaActual < 20; 
	}
	
	@Override
	public double getCostoPorMinuto() {
		double costo;
		if (!this.esFinDeSemana() && esHoraPico()) {
			costo = costoMinutoExtraordinario;
		} else {
			costo = costoMinutoBasico;
		}
		return costo;
	}

	@Override
	public boolean esLocal() {
		return true;
	}

}

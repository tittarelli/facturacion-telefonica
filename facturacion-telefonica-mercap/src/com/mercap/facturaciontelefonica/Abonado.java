package com.mercap.facturaciontelefonica;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Abonado {
	private List<Llamada> llamadas = new ArrayList<Llamada>();
	private Abono abono;
	private String nombre;
	
	public Abonado(String nombre, Abono abono) {
		this.abono = abono;
		this.nombre = nombre;
	}

	public Abono getAbono() {
		return this.abono;
	}
	
	public String getNombre() {
		return this.nombre;
	}

	//Este m�todo devuelve una lista de las llamadas que se hicieron con destino local y dentro de un determinado periodo/mes.
	public List<Llamada> getLlamadasLocales(int periodo) {
		return llamadas.stream()
				.filter(llamada -> llamada.esLocal() && llamada.esDelPeriodo(periodo))
				.collect(Collectors.toList());
	}
	
	public List<Llamada> getLlamadasNoLocales(int periodo) {
		return llamadas.stream()
				.filter(llamada -> !llamada.esLocal() && llamada.esDelPeriodo(periodo))
				.collect(Collectors.toList());
	}
	
	// Metodo para agregar una llamada a la lista de llamadas
	public void nuevaLlamada(String receptor, int duracion) throws Exception {
		this.llamadas.add(new Llamada(receptor, duracion));
	}

}

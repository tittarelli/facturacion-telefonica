package com.mercap.facturaciontelefonica;

import java.time.LocalDate;

public class Llamada {
	private String receptor;
	private int duracion;
	private TipoDeLlamada tipoDeLlamada;
	private LocalDate fecha;
	
	public Llamada(String receptor, int duracion) throws Exception {
		if(!this.esReceptorValido(receptor)){throw new Exception("El n�mero ingresado no corresponde a un cliente en servicio");};
		this.receptor = receptor.replace(" ", "");
		this.duracion = duracion;
		this.fecha = LocalDate.now();
		this.setTipoDeLlamada();
	}
	
	//Este m�todo compara la cadena receptor con una expresi�n que determina si est� hecha solo de esos caracteres y si tiene 6 o m�s d�gitos. 
	private boolean esReceptorValido(String receptor) {
		return receptor.matches("[0-9*+ -]*") && receptor.length() >= 6;
	}

	public String getReceptor() {
		return receptor;
	}

	public double getCosto() {
		return this.duracion * tipoDeLlamada.getCostoPorMinuto();
	}
	
	//Este m�todo setea el tipo de llamada en funci�n del patr�n establecido en la etapa de dise�o del proyecto.
	private void setTipoDeLlamada() throws Exception {
		if (receptor.length() <= 8) {
			tipoDeLlamada = new LlamadaLocal();
		} else if (receptor.chars().filter(c -> c == '-').count() > 1) {
			tipoDeLlamada = new LlamadaInternacional(this.codigo());
		} else {
			tipoDeLlamada = new LlamadaNacional(this.codigo());
		}
	}

	//Esta m�todo corta la cadena en el primer - que encuentre y devuelve parseado el primer elemento del array que se genera.
	private int codigo() {
		String[] codigo = receptor.split("-");
		return Integer.parseInt(codigo[0]); 
	}
	
	public boolean esLocal() {
		return tipoDeLlamada.esLocal();
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public boolean esDelPeriodo(int periodo) {
		return fecha.getMonthValue() == periodo;
	}
}
